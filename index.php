<?php
require "animal.php";
require "ape.php";
require "frog.php";

$sheep = new Animal("shaun");

echo "Name : ".$sheep->name."<br>";// "shaun"
echo "Legs :".$sheep->legs."<br>"; // 4
echo "cold blooded :".$sheep->cold_blooded."<br><br>";; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$sungokong = new Ape("kera sakti");
echo "Name : ".$sungokong->name."<br>";// "shaun"
echo "Legs :".$sungokong->legs."<br>"; // 4
echo "cold blooded :".$sungokong->cold_blooded."<br>";
echo $sungokong->yell()."<br><br>";;
// "Auooo"

$kodok = new Frog("buduk");
echo "Name : ".$kodok->name."<br>";// "shaun"
echo "Legs :".$kodok->legs."<br>";; // 4
echo "cold blooded :".$kodok->cold_blooded."<br>";;
$kodok->jump() ; // "hop hop"

?>